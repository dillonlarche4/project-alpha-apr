from django.urls import path
from accounts.views import user_login, logout_view, user_signup

urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", logout_view, name="logout"),
    path("signup/", user_signup, name="signup"),
]
